<?php

/**
 * @file
 * Exclude (deny access) to fields on a per domain basis under domain access.
 *
 * @copyright (C) Copyright 2010 Palantir.net
 */

/**
 * Implementation of hook_menu().
 */
function domain_field_access_menu() {
  $items['admin/build/domain/field-access/%'] = array(
    'title' => t('Field Access'),
    'description' => t('Select fields for each domain.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('domain_field_access_form', 4),
    'access arguments' => array('administer domain field access'),
    );
  return $items;
}

/**
 * Implementation of hook_field_access().
 */
function domain_field_access_field_access($op, $field, $account, $node = NULL) {
  $_domain = domain_get_domain();
  $domain_id = $_domain['domain_id'];
  $excludes = domain_field_access_get($domain_id);

  // If none have been selected all have access to this domain.
  if (empty($excludes)) {
    return TRUE;
  }

  // If all is set exclude all fields.
  if (in_array('all', $excludes)) {
    return FALSE;
  }

  // Exclude the individual field being checked.
  if (!in_array($field['field_name'], $excludes)) {
    return FALSE;
  }
}

/**
 * Implementation of hook_domainlinks().
 */
function domain_field_access_domainlinks($domain) {
  $links = array();

  if (user_access('administer domain field access')) {
    // Add a link to the field settings.
    $links[] = array(
      'title' => t('Field Access'),
      'path' => 'admin/build/domain/field-access/'. $domain['domain_id'],
    );
  }
  return $links;
}

/**
 * Form Callback; A settings form to 
 */
function domain_field_access_form(&$form_state, $domain_id) {
  $domain = domain_load($domain_id);
  $options['all'] = t('Restrict all fields.');
  // Get content types 
  foreach (content_fields() as $field) {
    $options[$field['field_name']] = $field['field_name'];
  }
  $form['domain_field_access'] = array(
    '#title' => t('Select the fields for %domain.', array('%domain' => $domain['path'])),
    '#type' => 'checkboxes',
    '#options' => $options,
    '#default_value' => domain_field_access_form_options_default($domain_id),
    '#description' => t('All content types will be available to this domain if none are selected above.'),
    );
  $form['domain_id'] = array(
    '#type' => 'value',
    '#value' => $domain_id,
    );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    );
  return $form;
}

/**
 * Submit handler for domain_content_types_form
 */
function domain_field_access_form_submit(&$form, &$form_state) {
  $domain_id = $form_state['values']['domain_id'];
  $fields = array();
  $options = $form_state['values']['domain_field_access'];
  foreach ($options as $k => $v) {
    if (!empty($v)) {
      $fields[] = $k;
    }
  }
  domain_field_access_set($domain_id, $fields);
  drupal_set_message(t('The settings have been saved.'));
}

/**
 * Store the fields being excluded from a domain.
 *
 * @param int $domain_id
 *   The id for the domain we are saving data for.
 * @param array $fields
 *   An array of field names to exclude for the given domain. A key of 'all'
 *   will exclude all fields for a domain.
 *
 * @return array
 *   An array of config details
 */
function domain_field_access_set($domain_id, $fields = NULL) {
  static $cache = array();

  if (is_null($fields) && !isset($cache[$domain_id])) {
    $settings = db_result(db_query("SELECT data FROM {domain_field_access} WHERE domain_id = %d", $domain_id));
    if (!empty($settings)) {
      $cache[$domain_id] = unserialize($settings);
    }
  }
  elseif (!is_null($fields)) {
    $settings = serialize($fields);
    $count = db_result(db_query("SELECT COUNT(domain_id) FROM {domain_field_access} WHERE domain_id = %d", $domain_id));
    if ($count) {
      db_query("UPDATE {domain_field_access} SET data = '%s' WHERE domain_id = %d", $settings, $domain_id);
    }
    else {
      db_query("INSERT INTO {domain_field_access} (domain_id, data) VALUES (%d, '%s')", $domain_id, $settings);
    }
    $cache[$domain_id] = $fields;
  }

  return isset($cache[$domain_id]) ? $cache[$domain_id] : array();
}


/**
 * Get the fields being excluded from a domain.
 *
 * @param int $domain_id
 *   The id for the domain we are getting data for.
 */
function domain_field_access_get($domain_id) {
  return domain_field_access_set($domain_id);
}

/**
 * Get the default values for the form.
 *
 * @param int $domain_id
 *   The id for the domain we are saving data for.
 */
function domain_field_access_form_options_default($domain_id) {
  $return = array();
  $options = domain_field_access_get($domain_id);
  foreach ($options as $field) {
    $return[$field] = $field;
  }
  return $return;
}

/**
 * Implementation of hook_perm().
 */
function domain_field_access_perm() {
  return array('administer domain field access');
}